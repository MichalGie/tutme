import random
import string

def generate_token(length=32):
	return ''.join(random.choice(string.ascii_letters + string.digits) for x in range(length))
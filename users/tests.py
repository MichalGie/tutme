from django.test import TestCase

# Create your tests here.

from datetime import datetime, timedelta
from django.contrib import auth
from django.contrib.auth import get_user_model
from django.contrib.messages import get_messages
from django.urls import reverse
from django.test import Client
from django.template.defaultfilters import slugify
from django.utils.timezone import now

from .models import Announcement, Group
from .messages import USERS_MESSAGES
from courses.models import Course

def create_group():
	group = Group.objects.create(name='3ID12B')
	return group

def create_user(active=False, staff=False, profile=False):
	User = get_user_model()
	user = User.objects.create_user(email='michal@mail.com', password='haslo123', is_active=active, is_staff=staff)

	if profile:
		user.first_name = 'michal'
		user.last_name = 'grudzien'
		user.index = 784557
		user.group = create_group()
		user.save()

	return user


class UserRegisterMethodTests(TestCase):

	def test_user_register_no_user_data(self):
		response = self.client.post(reverse('register'), {})
		self.assertEqual(response.status_code, 200)
		self.assertEqual(response.context['form']['email'].errors, [u'Podaj email.'])
		self.assertEqual(response.context['form']['password1'].errors, [u'Podaj hasło.'])
		self.assertEqual(response.context['form']['password2'].errors, [u'Podaj ponownie hasło.'])
		messages = [m.message for m in get_messages(response.wsgi_request)]
		self.assertIn(USERS_MESSAGES['REGISTRATION_FAIL'], messages)

	def test_user_register_no_password(self):
		response = self.client.post(reverse('register'), {'email':'michal@op.pl'})
		self.assertEqual(response.status_code, 200)
		self.assertEqual(response.context['form']['password1'].errors, [u'Podaj hasło.'])
		self.assertEqual(response.context['form']['password2'].errors, [u'Podaj ponownie hasło.'])
		messages = [m.message for m in get_messages(response.wsgi_request)]
		self.assertIn(USERS_MESSAGES['REGISTRATION_FAIL'], messages)

	def test_user_register_password_too_short(self):
		response = self.client.post(reverse('register'), {'email':'michal@op.pl', 'password1':'abc'})
		self.assertEqual(response.status_code, 200)
		self.assertEqual(response.context['form']['password1'].errors, [u'Zbyt krótkie hasło ( min. 5 znaków ).'])
		self.assertEqual(response.context['form']['password2'].errors, [u'Podaj ponownie hasło.'])
		messages = [m.message for m in get_messages(response.wsgi_request)]
		self.assertIn(USERS_MESSAGES['REGISTRATION_FAIL'], messages)

	def test_user_register_password_too_long(self):
		response = self.client.post(reverse('register'), {'email':'michal@op.pl', 'password1':'abcdefasswasfa845345kj213k41jhjhfas'})
		self.assertEqual(response.status_code, 200)
		self.assertEqual(response.context['form']['password1'].errors, [u'Zbyt długi hasło ( max. 25 znaków ).'])
		self.assertEqual(response.context['form']['password2'].errors, [u'Podaj ponownie hasło.'])
		messages = [m.message for m in get_messages(response.wsgi_request)]
		self.assertIn(USERS_MESSAGES['REGISTRATION_FAIL'], messages)

	def test_user_register_no_password2(self):
		response = self.client.post(reverse('register'), {'email':'michal@op.pl', 'password1':'haslo123'})
		self.assertEqual(response.status_code, 200)
		self.assertEqual(response.context['form']['password1'].errors, [u'Hasła nie są identyczne!'])
		self.assertEqual(response.context['form']['password2'].errors, [u'Podaj ponownie hasło.'])
		messages = [m.message for m in get_messages(response.wsgi_request)]
		self.assertIn(USERS_MESSAGES['REGISTRATION_FAIL'], messages)

	def test_user_register_not_the_same_passwords(self):
		response = self.client.post(reverse('register'), {'email':'michal@op.pl', 'password1':'haslo123', 'password2':'haslo143'})
		self.assertEqual(response.status_code, 200)
		self.assertEqual(response.context['form']['password1'].errors, [u'Hasła nie są identyczne!'])
		messages = [m.message for m in get_messages(response.wsgi_request)]
		self.assertIn(USERS_MESSAGES['REGISTRATION_FAIL'], messages)

	def test_user_register_no_email_no_confirm_password(self):
		response = self.client.post(reverse('register'), {'password1':'abcdefa'})
		self.assertEqual(response.status_code, 200)
		self.assertEqual(response.context['form']['email'].errors, [u'Podaj email.'])
		self.assertEqual(response.context['form']['password1'].errors, [u'Hasła nie są identyczne!'])
		self.assertEqual(response.context['form']['password2'].errors, [u'Podaj ponownie hasło.'])
		messages = [m.message for m in get_messages(response.wsgi_request)]
		self.assertIn(USERS_MESSAGES['REGISTRATION_FAIL'], messages)

	def test_user_register_wrong_email(self):
		response = self.client.post(reverse('register'), {'email' : 'a.pl', 'password1':'abcdefa', 'password1':'abcdefa'})
		self.assertEqual(response.status_code, 200)
		self.assertEqual(response.context['form']['email'].errors, [u'Podaj prawidłowy email.'])
		messages = [m.message for m in get_messages(response.wsgi_request)]
		self.assertIn(USERS_MESSAGES['REGISTRATION_FAIL'], messages)

	def test_user_register_right_passwords(self):
		response = self.client.post(reverse('register'), {'email':'michal@op.pl', 'password1':'haslo123', 'password2':'haslo123'})
		self.assertEqual(response.status_code, 302)
		self.assertEqual(response['Location'], reverse('login'))
		messages = [m.message for m in get_messages(response.wsgi_request)]
		self.assertIn(USERS_MESSAGES['REGISTRATION_SUCCESS'], messages)


class UserLoginMethodTests(TestCase):

	def test_user_login_no_form_fields(self):
		response = self.client.post(reverse('login'), {})
		self.assertEqual(response.status_code, 200)
		self.assertEqual(response.context['form']['email'].errors, [u'Podaj adres email.'])
		self.assertEqual(response.context['form']['password'].errors, [u'Podaj hasło.'])
		messages = [m.message for m in get_messages(response.wsgi_request)]
		self.assertIn(USERS_MESSAGES['USER_LOGIN_ERROR'], messages)

	def test_user_login_no_form_data(self):
		response = self.client.post(reverse('login'), {'email': '', 'password': ''})
		self.assertEqual(response.status_code, 200)
		self.assertEqual(response.context['form']['email'].errors, [u'Podaj adres email.'])
		self.assertEqual(response.context['form']['password'].errors, [u'Podaj hasło.'])
		messages = [m.message for m in get_messages(response.wsgi_request)]
		self.assertIn(USERS_MESSAGES['USER_LOGIN_ERROR'], messages)

	def test_user_login_no_password_field(self):
		response = self.client.post(reverse('login'), {'email':'michal@op.pl'})
		self.assertEqual(response.status_code, 200)
		self.assertEqual(response.context['form']['password'].errors, [u'Podaj hasło.'])
		messages = [m.message for m in get_messages(response.wsgi_request)]
		self.assertIn(USERS_MESSAGES['USER_LOGIN_ERROR'], messages)

	def test_user_login_no_password_data(self):
		response = self.client.post(reverse('login'), {'email':'michal@op.pl', 'password':''})
		self.assertEqual(response.status_code, 200)
		self.assertEqual(response.context['form']['password'].errors, [u'Podaj hasło.'])
		messages = [m.message for m in get_messages(response.wsgi_request)]
		self.assertIn(USERS_MESSAGES['USER_LOGIN_ERROR'], messages)

	def test_user_login_no_email_field(self):
		response = self.client.post(reverse('login'), {'password':'haslo123'})
		self.assertEqual(response.status_code, 200)
		self.assertEqual(response.context['form']['email'].errors, [u'Podaj adres email.'])
		messages = [m.message for m in get_messages(response.wsgi_request)]
		self.assertIn(USERS_MESSAGES['USER_LOGIN_ERROR'], messages)

	def test_user_login_no_email_data(self):
		response = self.client.post(reverse('login'), {'email':'', 'password':'haslo123'})
		self.assertEqual(response.status_code, 200)
		self.assertEqual(response.context['form']['email'].errors, [u'Podaj adres email.'])
		messages = [m.message for m in get_messages(response.wsgi_request)]
		self.assertIn(USERS_MESSAGES['USER_LOGIN_ERROR'], messages)

	def test_user_login_invalid_emial(self):
		response = self.client.post(reverse('login'), {'email':'aa', 'password':'haslo123'})
		self.assertEqual(response.status_code, 200)
		self.assertEqual(response.context['form']['email'].errors, [u'Podaj poprawny adres email.'])
		messages = [m.message for m in get_messages(response.wsgi_request)]
		self.assertIn(USERS_MESSAGES['USER_LOGIN_ERROR'], messages)

	def test_user_login_invalid_emial_invalid_password(self):
		response = self.client.post(reverse('login'), {'email':'aa', 'password':'hasl'})
		self.assertEqual(response.status_code, 200)
		self.assertEqual(response.context['form']['email'].errors, [u'Podaj poprawny adres email.'])
		self.assertEqual(response.context['form']['password'].errors, [u'Zbyt krótkie hasło ( min. 5 znaków ).'])
		messages = [m.message for m in get_messages(response.wsgi_request)]
		self.assertIn(USERS_MESSAGES['USER_LOGIN_ERROR'], messages)

	def test_user_login_no_user(self):
		response = self.client.post(reverse('login'), {'email':'michal@op.pl', 'password':'haslo123'})
		self.assertEqual(response.status_code, 200)
		messages = [m.message for m in get_messages(response.wsgi_request)]
		self.assertIn(USERS_MESSAGES['USER_LOGIN_NOT_FOUND'], messages)

	def test_user_login_wrong_passowrd(self):
		user = get_user_model()
		user.objects.create_user(email='michal@op.pl', password='haslo123')
		response = self.client.post(reverse('login'), {'email':'michal@op.pl', 'password':'hasloaaaa'})
		self.assertEqual(response.status_code, 200)
		messages = [m.message for m in get_messages(response.wsgi_request)]
		self.assertIn(USERS_MESSAGES['USER_LOGIN_FAIL'], messages)

	def test_user_login_no_active(self):
		user = get_user_model()
		user.objects.create_user(email='michal@op.pl', password='haslo123')
		response = self.client.post(reverse('login'), {'email':'michal@op.pl', 'password':'haslo123'})
		self.assertEqual(response.status_code, 200)
		messages = [m.message for m in get_messages(response.wsgi_request)]
		self.assertIn(USERS_MESSAGES['USER_NOT_ACTIVE'], messages)

	def test_user_login_success(self):
		user = get_user_model()
		user.objects.create_user(email='michal@op.pl', password='haslo123', is_active=True)
		response = self.client.post(reverse('login'), {'email':'michal@op.pl', 'password':'haslo123'})
		self.assertEqual(response.status_code, 302)
		self.assertEqual(response['Location'], reverse('home'))
		messages = [m.message for m in get_messages(response.wsgi_request)]
		self.assertIn(USERS_MESSAGES['USER_LOGIN_SUCCESS'], messages)

class UserLogoutMethodTests(TestCase):

	def test_logout(self):
		user = get_user_model()
		self.assertFalse(auth.get_user(self.client).is_authenticated())
		user.objects.create_user(email='michal@op.pl', password='haslo123', is_active=True)
		self.client.login(email='michal@op.pl', password='haslo123')
		self.assertTrue(auth.get_user(self.client).is_authenticated())
		self.client.logout()
		self.assertFalse(auth.get_user(self.client).is_authenticated())


class UserAdminAccessMethodTests(TestCase):

	def test_is_staff_user_access_panel(self):
		user = get_user_model()
		user.objects.create_user(email='michal@op.pl', password='haslo123', is_active=True, is_staff=True)
		self.client.login(email='michal@op.pl', password='haslo123')
		self.assertTrue(auth.get_user(self.client).is_authenticated())
		self.assertTrue(auth.get_user(self.client).is_staff)
		response = self.client.get(reverse('admin:index'))
		self.assertEqual(response.status_code, 200)

	def test_is_staff_no_active_user_access_panel(self):
		user = get_user_model()
		user.objects.create_user(email='michal@op.pl', password='haslo123', is_active=False, is_staff=True)
		self.client.login(email='michal@op.pl', password='haslo123')
		self.assertFalse(auth.get_user(self.client).is_authenticated())
		response = self.client.get(reverse('admin:index'))
		self.assertEqual(response.status_code, 302)

	def test_no_is_staff_is_active_info_user_access_panel(self):
		user = get_user_model()
		user.objects.create_user(email='michal@op.pl', password='haslo123')
		self.client.login(email='michal@op.pl', password='haslo123')
		self.assertFalse(auth.get_user(self.client).is_authenticated())
		response = self.client.get(reverse('admin:index'))
		self.assertEqual(response.status_code, 302)

	def test_no_staff_info_user_access_panel(self):
		user = get_user_model()
		user.objects.create_user(email='michal@op.pl', password='haslo123', is_active=True)
		self.client.login(email='michal@op.pl', password='haslo123')
		self.assertTrue(auth.get_user(self.client).is_authenticated())
		self.assertFalse(auth.get_user(self.client).is_staff)
		response = self.client.get(reverse('admin:index'))
		self.assertEqual(response.status_code, 302)

	def test_no_staff_user_access_report_generation(self):
		user = get_user_model()
		user.objects.create_user(email='michal@op.pl', password='haslo123', is_active=True)
		self.client.login(email='michal@op.pl', password='haslo123')
		self.assertTrue(auth.get_user(self.client).is_authenticated())
		self.assertFalse(auth.get_user(self.client).is_staff)

		course = Course.objects.create(name='test')

		response = self.client.get(reverse('generate-course-raport', kwargs={'course_slug':course.slug,}))
		self.assertEqual(response.status_code, 302)
		self.assertEqual(response['Location'], '%s?next=%s' % (reverse('admin:login'), reverse('generate-course-raport', kwargs={'course_slug':course.slug,})))

	def test_staff_user_access_report_generation(self):
		user = get_user_model()
		user.objects.create_user(email='michal@op.pl', password='haslo123', is_active=True, is_staff=True)
		self.client.login(email='michal@op.pl', password='haslo123')
		self.assertTrue(auth.get_user(self.client).is_authenticated())
		self.assertTrue(auth.get_user(self.client).is_staff)

		course = Course.objects.create(name='test')

		response = self.client.get(reverse('generate-course-raport', kwargs={'course_slug':course.slug,}))
		self.assertEqual(response.status_code, 200)
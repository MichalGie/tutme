# -*- coding: utf-8 -*-
# Generated by Django 1.11.5 on 2018-02-10 15:20
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('users', '0003_auto_20180209_0628'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='announcement',
            options={'verbose_name': 'Ogłoszenie', 'verbose_name_plural': '5. Ogłoszenia'},
        ),
        migrations.AlterModelOptions(
            name='group',
            options={'verbose_name': 'Grupa dziekańska', 'verbose_name_plural': '2. Grupy dziekańskie'},
        ),
        migrations.AlterModelOptions(
            name='usercoursedetail',
            options={'verbose_name': 'Kurs użytkownika', 'verbose_name_plural': '3. Kursy użytkowników'},
        ),
        migrations.AlterModelOptions(
            name='userlecturedetail',
            options={'verbose_name': 'Zajęcia użytkownika', 'verbose_name_plural': '4. Zajęcia użytkowników'},
        ),
    ]

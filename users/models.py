from datetime import datetime

from django.core.validators import MaxLengthValidator, MinLengthValidator, RegexValidator
# from django.contrib.auth.models import User
from django.contrib.auth.base_user import AbstractBaseUser, BaseUserManager
from django.contrib.auth.models import PermissionsMixin
from django.db import models
from django.db.models import Q
from django.db.models.signals import post_save, pre_save
from django.contrib.auth.models import AbstractUser
from django.dispatch import receiver

# Create your models here.
from courses.models import Course, Lecture, Page, UserTaskDetail

class UserManager(BaseUserManager):
	use_in_migrations = True

	def _create_user(self, email, password, **extra_fields):
		if not email:
			raise ValueError('No mail is given')
		email = self.normalize_email(email)
		user = self.model(email=email, **extra_fields)
		user.set_password(password)
		user.save(using=self._db)
		return user

	def create_user(self, email, password=None, **extra_fields):
		extra_fields.setdefault('is_superuser', False)
		return self._create_user(email, password, **extra_fields)

	def create_superuser(self, email, password, **extra_fields):
		extra_fields.setdefault('is_superuser', True)
		extra_fields.setdefault('is_staff', True)
		extra_fields.setdefault('is_active', True)
		extra_fields.setdefault('role', 'T')

		if extra_fields.get('is_superuser') is not True:
			raise ValueError('Superuser must have is_superuser=True.')

		return self._create_user(email, password, **extra_fields)

class User(AbstractBaseUser, PermissionsMixin):
	email 		= 	models.EmailField(blank=False, null=False, unique=True, verbose_name='Email')
	first_name	=	models.CharField(max_length=32, blank=False, null=True, verbose_name='Imie')
	last_name 	=	models.CharField(max_length=32, blank=False, null=True, verbose_name='Nazwisko')

	STUDENT = 'S'
	TEACHER = 'T'
	ROLE_CHOICES = (
		(STUDENT, 'Student'),
		(TEACHER, 'Prowadzący'),
	)

	index 				= models.CharField(max_length=6, blank=False, null=True, validators=[RegexValidator(r'^\d{1,10}$'), MinLengthValidator(5), MaxLengthValidator(6)], verbose_name="Numer indeksu",
							error_messages ={
								'blank' 		: 	'Uzupełnij swój numer indeksu.',
								'invalid'		:	'Wprowadź poprawny number indeksu.',
								'min_length'	: 	'Niepoprawny number indeksu ( min. długość: 5 ).',
								'max_length'	: 	'Niepoprawny number indeksu ( max. długość: 5 ).',
							},
						)
	group 				= models.ForeignKey('Group', on_delete=models.CASCADE, blank=False, null=True, verbose_name="Grupa",
							error_messages ={
								'blank' : 'Wybierz swoją grupę dziekańską',
							},
						)

	is_active 			= models.BooleanField(default=False, verbose_name='Aktywny')
	is_staff			= models.BooleanField(default=False, verbose_name='Super user')
	activation_token 	= models.CharField(max_length=32, blank=True, null=True, verbose_name='Token aktywacyjny')
	reset_token 		= models.CharField(max_length=32, blank=True, null=True, verbose_name='Token resetujący hasło')
	role 				= models.CharField(max_length=1, choices=ROLE_CHOICES, default=STUDENT, blank=False, null=False, verbose_name="Typ konta")

	updated = models.DateTimeField(auto_now=True, verbose_name='Modyfikowano')
	created = models.DateTimeField(auto_now_add=True, verbose_name='Utworzono')

	objects = UserManager()

	USERNAME_FIELD = 'email'
	REQUIRED_FIELDS = []

	def __str__(self):
		return self.get_full_name()

	class Meta:
		verbose_name = 'Użytkownik'
		verbose_name_plural = '1. Użytkownicy'

	def profile_is_complete(self):
		if self.first_name and self.last_name and self.index and self.group:
			return True
		else:
			return False

	def get_short_name(self):
		if self.profile_is_complete():
			return self.first_name
		else:
			return self.email

	def get_full_name(self):
		if self.profile_is_complete():
			return self.last_name + ' ' + self.first_name
		else:
			return self.email
	get_full_name.short_description = 'Użytkownik'


class UserCourseDetail(models.Model):
	user 	= models.ForeignKey(User, related_name='courses_user', verbose_name='Użytkownik', on_delete=models.CASCADE)
	course 	= models.ForeignKey(Course, related_name='user_courses', verbose_name='Kurs', on_delete=models.CASCADE)

	extra 	= models.CharField(max_length=64, blank=True, null=True)

	updated = models.DateTimeField(auto_now=True, verbose_name='Modyfikowano')
	created = models.DateTimeField(auto_now_add=True, verbose_name='Utworzono')

	def __str__(self):
		return self.course.name

	def get_user_points(self):
		lectures = UserCourseDetail.objects.filter(user=self.user, course=self.course)
		points = 0

		for lecture in lectures:
			for lec in lecture.lectures_in_course.all():
				points += lec.get_points()

		return points

	def get_user(self):
		return self.user.get_full_name()


	class Meta:
		verbose_name = 'Kurs użytkownika'
		verbose_name_plural = '3. Kursy użytkowników'
		unique_together = ('user','course')


class UserLectureDetail(models.Model):
	course_detail 	= models.ForeignKey(UserCourseDetail, related_name='lectures_in_course', verbose_name='Kurs', on_delete=models.CASCADE)
	lecture 		= models.ForeignKey(Lecture, related_name='user_lecture', verbose_name='Zajęcia', on_delete=models.CASCADE)

	points			= models.FloatField(default=5, blank=False, null=False, verbose_name='Punkty')
	task 			= models.TextField(blank=False, null=False, verbose_name='Zadanie')

	SENDED = 'S'
	APPROVED = 'A'
	DENIED = 'D'

	USER_LECTURE_CHOICES = (
		(SENDED, 'Przesłano'),
		(APPROVED, 'Zatwierzone'),
		(DENIED, 'Odrzucone'),
	)

	status 	= models.CharField(max_length=1, choices=USER_LECTURE_CHOICES, default=SENDED, verbose_name='Status')

	updated = models.DateTimeField(auto_now=True, verbose_name='Modyfikowano')
	created = models.DateTimeField(auto_now_add=True, verbose_name='Utworzono')

	def __str__(self):
		return self.lecture.name

	class Meta:
		verbose_name = "Zajęcia użytkownika"
		verbose_name_plural = "4. Zajęcia użytkowników"
		unique_together = ('course_detail','lecture')

	
	def get_task(self):
		user_task = UserTaskDetail.objects.filter(
			Q(task__lecture=self.lecture) & Q(user=self.course_detail.user)
			).first()
		return user_task

	def get_user(self):
		return self.course_detail.user.get_full_name()
	get_user.short_description = 'Użytkownik'

	def get_points(self):
		if self.status is self.APPROVED:
			return self.points
		else:
			return 0

	def get_status(self):
		return self.get_status_display()


class Group(models.Model):
	class Meta:
		verbose_name = 'Grupa dziekańska'
		verbose_name_plural = '2. Grupy dziekańskie'

	def __str__(self):
		return self.name

	MONDAY = 'M'
	TUESDAY = 'T'
	WEDNESDAY = 'W'
	THURSDAY = 'T'
	FRIDAY = 'F'
	SATURDAY = 'S'
	SUNDAY = 'Z'

	GROUP_DAY_LECTURES_CHOICES = (
			(MONDAY, 'Poniedziałek'),
			(TUESDAY, 'Wtorek'),
			(WEDNESDAY, 'Środa'),
			(THURSDAY, 'Czwartek'),
			(FRIDAY, 'Piątek'),
			(SATURDAY, 'Sobota'),
			(SUNDAY, 'Niedziela'),
		)

	H1 = '8:00'
	H2 = '9:50'
	H3 = '11:40'
	H4 = '13:30'
	H5 = '15:20'
	H6 = '17:10'
	H7 = '19:00'
	GROUP_TIME_LECTURE_CHOICES = (
		(datetime.strptime(H1, "%H:%M").time(), H1),
		(datetime.strptime(H2, "%H:%M").time(), H2),
		(datetime.strptime(H3, "%H:%M").time(), H3),
		(datetime.strptime(H4, "%H:%M").time(), H4),
		(datetime.strptime(H5, "%H:%M").time(), H5),
		(datetime.strptime(H6, "%H:%M").time(), H6),
		(datetime.strptime(H7, "%H:%M").time(), H7),
		)

	name = models.CharField(max_length=16, blank=False, null=False, verbose_name="Nazwa")
	day = models.CharField(max_length=1, choices=GROUP_DAY_LECTURES_CHOICES, default=MONDAY, verbose_name="Dzień")
	time = models.TimeField(choices=GROUP_TIME_LECTURE_CHOICES, default=H1, verbose_name="Godzina")


class Announcement(models.Model):
	user 	= models.ForeignKey(User, on_delete=models.CASCADE, verbose_name='Użytkownik')
	course 	= models.ForeignKey(Course, on_delete=models.CASCADE, verbose_name='Kurs')

	text 	= models.TextField(blank=False, null=False, verbose_name='Treść')

	updated = models.DateTimeField(auto_now=True, verbose_name='Modyfikowano')
	created = models.DateTimeField(auto_now_add=True, verbose_name='Utworzono')

	class Meta:
		verbose_name = 'Ogłoszenie'
		verbose_name_plural = '5. Ogłoszenia'
		ordering = ['-pk']

	def __str__(self):
		return self.course.name + '( ' + self.updated.strftime("%Y.%m.%d %H:%M") + ' )' + ' przez ' + self.user.get_full_name()

	@property
	def get_author(self):
		return self.user.get_full_name()
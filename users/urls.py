from django.conf.urls import url

from .views import LoginFormView, ResetPasswordFormView, ResetPasswordUpdateView, RegisterFormView, UserUpdateView, UserDetailView, ActivateView, LogoutView

urlpatterns = [
		url(r'^login/$', LoginFormView.as_view(), name='login'),
		url(r'^logout/$', LogoutView.as_view(), name='logout'),
		url(r'^register/$', RegisterFormView.as_view(), name='register'),
		url(r'^profile/$', UserDetailView.as_view(), name='profile'),
		url(r'^profile/edit/$', UserUpdateView.as_view(), name='profile-edit'),
		url(r'^activate/(?P<token>[\w-]+)/$', ActivateView.as_view(), name='activate'),
		url(r'^reset-password/$', ResetPasswordFormView.as_view(), name='reset-password'),
		url(r'^reset-password/(?P<token>[\w-]+)/$', ResetPasswordUpdateView.as_view(), name='reset-password-link'),
		url(r'^$', UserDetailView.as_view(), name='profile2'),
]
from django import forms
from django.core.validators import EmailValidator, MaxLengthValidator, MinLengthValidator
from django.contrib.auth import get_user_model
from django.forms import ModelForm
from django.forms.widgets import EmailInput, PasswordInput
from .models import  UserLectureDetail

class UserLectureDetailForm(ModelForm):
	class Meta:
		model = UserLectureDetail
		fields = ['task']

class LoginForm(forms.Form):
	email = forms.CharField(widget=EmailInput, required=True, validators=[EmailValidator()],
			error_messages = {
				'required'	:	'Podaj adres email.',
				'invalid' 	: 	'Podaj poprawny adres email.',
			},
		)
	password = forms.CharField(widget=PasswordInput, required=True, validators=[MinLengthValidator(5), MaxLengthValidator(25)],
				error_messages = {
					'required'		:	'Podaj hasło.',
					'min_length'	: 	'Zbyt krótkie hasło ( min. 5 znaków ).',
					'max_length'	:	'Zbyt długi hasło ( max. 25 znaków ).',
				}
			)

class RegisterForm(forms.ModelForm):

	password1 = forms.CharField(widget=PasswordInput, required=True, validators=[MinLengthValidator(5), MaxLengthValidator(25)],
				error_messages = {
					'required'		:	'Podaj hasło.',
					'min_length'	: 	'Zbyt krótkie hasło ( min. 5 znaków ).',
					'max_length'	:	'Zbyt długi hasło ( max. 25 znaków ).',
				}
			)
	password2 = forms.CharField(widget=PasswordInput, required=True, validators=[MinLengthValidator(5), MaxLengthValidator(25)],
				error_messages = {
					'required'		:	'Podaj ponownie hasło.',
				}
			)

	class Meta:
		model = get_user_model()
		fields = ['email', 'password1', 'password2' ]
		error_messages = {
			'email' : {
				'required' 	: 'Podaj email.',
				'invalid'	: 'Podaj prawidłowy email.',
			},
		}

	def clean(self):
		cleaned_data = super(RegisterForm, self).clean()

		if cleaned_data.get('password1') != cleaned_data.get('password2'):
			self.add_error('password1', 'Hasła nie są identyczne!')
			
		return cleaned_data

class ResetPasswordForm(forms.Form):
	email = forms.CharField(widget=EmailInput, required=True,
			error_messages = {
				'required' 	: 'Podaj email.',
				'invalid'	: 'Podaj prawidłowy email.',
			}
		)


class UserResetPasswordForm(forms.ModelForm):
	password2 = forms.CharField(widget=PasswordInput, required=True,
				error_messages = {
					'required'		:	'Podaj hasło.',
					'min_length'	: 	'Zbyt krótkie hasło ( min. 5 znaków ).',
					'max_length'	:	'Zbyt długi hasło ( max. 25 znaków ).',
				}
			)

	class Meta:
		model = get_user_model()
		fields = ['password', ]
		widgets = {
			'password': forms.PasswordInput(),
		}

	def clean(self):
		cleaned_data = super(UserResetPasswordForm, self).clean()

		if cleaned_data.get('password') != cleaned_data.get('password2'):
			raise forms.ValidationError('Hasła nie są identyczne!')
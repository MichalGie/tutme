from django.contrib import admin
from django.contrib.auth.admin import UserAdmin

from .models import User, Group, UserLectureDetail, Announcement, UserCourseDetail


class CustomUserAdmin(admin.ModelAdmin):
	readonly_fields = ('updated', 'created')
	list_display = ('get_full_name', 'group', 'index', 'is_active', 'is_staff')

	fieldsets = (
		(None, {
			'fields': ('first_name', 'last_name', 'index', 'email', 'group', 'role')
		}),
		('Więcej', {
			'classes': ('collapse',),
			'fields': ('is_active', 'is_staff'),
		}),
		('Generowane automatycznie', {
			'classes': ('collapse',),
			'fields': ('activation_token', 'reset_token', 'updated', 'created'),
		}),
	)

class CustomGroupAdmin(admin.ModelAdmin):
	list_display = ('name', 'day', 'time')

	fieldsets = (
		(None, {
			'fields': ('name', 'day', 'time')
		}),
	)


class CustomUserCourseDetail(admin.ModelAdmin):
	readonly_fields = ('updated', 'created')
	list_display = ('user', 'course', 'updated')
	
	fieldsets = (
		(None, {
			'fields': ('user', 'course', 'extra')
		}),
		('Generowane automatycznie', {
			'classes': ('collapse',),
			'fields': ('updated', 'created'),
		}),
	)


class CustomUserLectureDetail(admin.ModelAdmin):
	readonly_fields = ('updated', 'created')
	list_display = ('get_user', 'lecture', 'course_detail', 'points', 'status')
	
	fieldsets = (
		(None, {
			'fields': ('course_detail', 'lecture', 'task', 'points', 'status')
		}),
		('Generowane automatycznie', {
			'classes': ('collapse',),
			'fields': ('updated', 'created'),
		}),
	)


class CustomAnnouncementAdmin(admin.ModelAdmin):
	readonly_fields = ('updated', 'created')
	list_display = ('user', 'course', 'updated')

	fieldsets = (
		(None, {
			'fields': ('user', 'course', 'text',)
		}),
		('Generowane automatycznie', {
			'classes': ('collapse',),
			'fields': ('updated', 'created'),
		}),
	)


admin.site.register(User, CustomUserAdmin)
admin.site.register(Group, CustomGroupAdmin)
admin.site.register(UserCourseDetail, CustomUserCourseDetail)
admin.site.register(UserLectureDetail, CustomUserLectureDetail)
admin.site.register(Announcement, CustomAnnouncementAdmin)
from django import template
from users.models import UserLectureDetail, UserCourseDetail
from courses.models import UserTaskDetail, USER_TASK_STATUS

register = template.Library()

@register.simple_tag
def get_user_lecture_status(user, lecture):

	try:
		user_lecture = UserLectureDetail.objects.get(course_detail__user=user, lecture=lecture)
		return user_lecture.get_status_display()
	except:
		return ''


@register.assignment_tag
def get_user_lecture_stats(user, lecture):
	try:
		user_lecture = UserLectureDetail.objects.get(course_detail__user=user, lecture=lecture)
		user_lecture_points = user_lecture.get_points()
		user_lecture_status = user_lecture.get_status()
	except:
		user_lecture_points = '---'
		user_lecture_status = 'Nie przesłano'

	try:
		user_task = UserTaskDetail.objects.get(user=user, task__lecture=lecture)
		user_task = user_task.get_status_display()
	except:
		user_task = USER_TASK_STATUS[0][1]

	ret = {'points': user_lecture_points, 'status': user_lecture_status, 'task' : user_task}
	return ret

@register.simple_tag
def get_user_lecture_points(user, lecture):
	try:
		user_lecture = UserLectureDetail.objects.get(course_detail__user=user, lecture=lecture)
		return user_lecture.get_points()
	except:
		return '---'

@register.simple_tag
def get_user_course_points(user, course):
	course = UserCourseDetail.objects.get(course=course, user=user)
	user_poitns = course.get_user_points()
	return user_poitns

from django.conf import settings
from django.contrib import messages
from django.contrib.auth import authenticate, get_user_model, login, logout
from django.contrib.auth.mixins import LoginRequiredMixin
# from django.contrib.auth.models import User
from django.core.mail import EmailMultiAlternatives
from django.core.urlresolvers import reverse
from django.shortcuts import render, redirect
from django.template import Context
from django.template.loader import get_template
from django.views import View
from django.views.generic.detail import DetailView
from django.views.generic.edit import FormView, CreateView, UpdateView
# Create your views here.

from courses.models import Course
from .forms import LoginForm, RegisterForm, ResetPasswordForm, UserResetPasswordForm
from .models import UserCourseDetail, UserLectureDetail
from .utils import generate_token
from .messages import USERS_MESSAGES

class LogoutRequiredMixin(View):
	def dispatch(self, request, *args, **kwargs):
		if request.user.is_authenticated():
			return redirect('/')
		return super(LogoutRequiredMixin, self).dispatch(request, *args, **kwargs)

class CustomLoginActivateRequiredMixin(View):
	def dispatch(self, request, *args, **kwargs):
		if request.user.is_authenticated():
			user = request.user
			if user.profile_is_complete() or user.is_staff:
				return super(CustomLoginActivateRequiredMixin, self).dispatch(request, *args, **kwargs)
			else:
				messages.add_message(request, messages.WARNING, USERS_MESSAGES['PROFILE_NOT_COMPLETE'])
				return redirect('profile-edit')
		else:
			messages.add_message(request, messages.WARNING, USERS_MESSAGES['LOGIN_REQUIRED'])
		return redirect('login')

class CustomLoginRequiredMixin(View):
	def dispatch(self, request, *args, **kwargs):
		if request.user.is_authenticated():
			return super(CustomLoginRequiredMixin, self).dispatch(request, *args, **kwargs)
		else:
			messages.add_message(request, messages.WARNING, USERS_MESSAGES['LOGIN_REQUIRED'])
		return redirect('login')
		

class RegisterFormView(LogoutRequiredMixin, FormView):
	form_class = RegisterForm
	# fields = ['first_name', 'last_name', 'email', 'password', ]
	template_name = 'users/register_form.html'
	success_url = '/'

	def post(self, request, *args, **kwargs):
		form = RegisterForm(request.POST or None)

		if form.is_valid():
			user_model = get_user_model()
			user = user_model.objects.create_user(email=form.cleaned_data['email'],
											password=form.cleaned_data['password1'],
										)

			token = generate_token()
			user.activation_token = token
			user.save()

			activation_link = "http://%s%s" % (settings.SITE_URL, reverse('activate', args=[token]))

			plaintext = get_template('emails/user_activation.txt')
			htmly     = get_template('emails/user_activation.html')

			d = {
				'activation_link'	: activation_link,
				'user_name' 		: form.cleaned_data['email'],
				}

			subject 	= 'Aktywacja konta na platformie tutMe'
			from_email 	= 'admin@tutme.pl'
			to 			= form.cleaned_data['email']

			text_content = plaintext.render(d)
			html_content = htmly.render(d)
			msg = EmailMultiAlternatives(subject, text_content, from_email, [to])
			msg.attach_alternative(html_content, "text/html")
			msg.send()

			messages.add_message(request, messages.SUCCESS, USERS_MESSAGES['REGISTRATION_SUCCESS'])
			return redirect('login')

		else:
			messages.add_message(request, messages.ERROR, USERS_MESSAGES['REGISTRATION_FAIL'])
			return self.render_to_response(self.get_context_data())


class LoginFormView(LogoutRequiredMixin, FormView):
	template_name = 'users/login_form.html'
	success_url = '/'
	form_class = LoginForm

	def post(self, request, *args, **kwargs):
		form = LoginForm(request.POST or None)
		if form.is_valid():
			email = form.cleaned_data['email']
			password = form.cleaned_data['password']

			get_user = get_user_model()

			try:
				user = get_user.objects.get(email=email)
			except:
				messages.add_message(request, messages.ERROR, USERS_MESSAGES['USER_LOGIN_NOT_FOUND'])
			else:
				if user.check_password(password):
					if user is not None:
						if user.is_active:
							login(request, user)
							messages.add_message(request, messages.INFO, USERS_MESSAGES['USER_LOGIN_SUCCESS'])
							return redirect('/')
						else:
							messages.add_message(request, messages.ERROR, USERS_MESSAGES['USER_NOT_ACTIVE'])
				else:
					messages.add_message(request, messages.ERROR, USERS_MESSAGES['USER_LOGIN_FAIL'])
		else:
			messages.add_message(request, messages.ERROR, USERS_MESSAGES['USER_LOGIN_ERROR'])
		return self.render_to_response(self.get_context_data())


class LogoutView(CustomLoginRequiredMixin, View):
	def get(self, request, *args, **kwargs):
		logout(request)
		messages.add_message(request, messages.INFO, USERS_MESSAGES['USER_LOGOUT'])
		return redirect('/')

class UserUpdateView(CustomLoginRequiredMixin, UpdateView):
	model = get_user_model()
	fields = ['first_name', 'last_name', 'index', 'group',]

	def get_success_url(self):
		return reverse('profile')
	
	def get_object(self):
		User = get_user_model()
		user = self.request.user
		obj = User.objects.get(pk=user.pk)
		return obj

	def form_valid(self, form):
		messages.add_message(self.request, messages.INFO, USERS_MESSAGES['USER_PROFILE_CHANGE_SUCCESS'])
		return super(UserUpdateView, self).form_valid(form)


class UserDetailView(CustomLoginRequiredMixin, DetailView):
	model = get_user_model()

	def get_object(self, *args, **kwargs):
		User = get_user_model()
		user = self.request.user
		return User.objects.get(pk=user.pk)

	def get_context_data(self, *args, **kwargs):
		user = self.request.user
		courses = UserCourseDetail.objects.filter(user=user).order_by('-pk')

		points = 0
		for course in courses:
			points += course.get_user_points()

		context = super(UserDetailView, self).get_context_data(*args, **kwargs)
		context['courses'] = courses

		return context


class ActivateView(LogoutRequiredMixin, View):
	def get(self, request, token, *args, **kwargs):
		try:
			user_model = get_user_model()
			user = user_model.objects.get(activation_token=token)
			user.is_active = True
			user.activation_token = None
			user.save()
			messages.add_message(request, messages.INFO, USERS_MESSAGES['USER_ACTIVATE_SUCCESS'])
		except:
			messages.add_message(request, messages.ERROR, USERS_MESSAGES['USER_ACTIVATE_ERROR'])
		return redirect('/')


class ResetPasswordFormView(LogoutRequiredMixin, FormView):
	template_name = 'users/reset_password_form.html'
	success_url = '/'
	form_class = ResetPasswordForm

	def post(self, request, *args, **kwargs):
		form =  ResetPasswordForm(request.POST or None)

		if form.is_valid():
			email = form.cleaned_data['email']
			get_user = get_user_model()

			try:
				user = get_user.objects.get(email=email)
			except:
				messages.add_message(request, messages.ERROR, USERS_MESSAGES['USER_LOGIN_NOT_FOUND'])
				return redirect('reset-password')

			# profile = Profile.objects.get(user=user)

			token = generate_token()

			user.reset_token = token
			user.save()

			reset_link = "http://%s%s" % (settings.SITE_URL, reverse('reset-password-link', args=[token]))

			plaintext = get_template('emails/user_reset_password.txt')
			htmly     = get_template('emails/user_reset_password.html')

			d = {
				'reset_link'	: reset_link,
				'user_name' 	: user.get_short_name(),
				}

			subject 	= 'Reset hasła na platformie tutMe'
			from_email 	= 'admin@tutme.pl'
			to 			= user.email

			text_content = plaintext.render(d)
			html_content = htmly.render(d)
			msg = EmailMultiAlternatives(subject, text_content, from_email, [to])
			msg.attach_alternative(html_content, "text/html")
			msg.send()

			messages.add_message(request, messages.INFO, USERS_MESSAGES['USER_REMIND_EMAIL_SENT'])

		return redirect('/')


class ResetPasswordUpdateView(LogoutRequiredMixin, UpdateView):
	model = get_user_model()
	form_class = UserResetPasswordForm
	template_name = 'users/reset_password_update.html'
	success_url = '/'

	def dispatch(self, request, token, *args, **kwargs):
			try:
				user_model = get_user_model()
				user = user_model.objects.get(reset_token=token)
			except:
				messages.add_message(request, messages.ERROR, USERS_MESSAGES['USER_RESET_PASSWORD_ERROR'])
				return redirect('/')
			else:
				return super(ResetPasswordUpdateView, self).dispatch(request, token, *args, **kwargs)
	
	def get_object(self):
			try:
				user_model = get_user_model()
				user = user_model.objects.get(reset_token=token)
				return user
			except:
				return None

	def post(self, request, token, *args, **kwargs):
		form = UserResetPasswordForm(request.POST or None)

		if form.is_valid():
			password = form.cleaned_data['password']
			password2 = form.cleaned_data['password2']

			user_model = get_user_model()
			user = user_model.objects.get(reset_token=token)
			user.set_password(password)
			user.reset_token = None
			user.save()

			messages.add_message(request, messages.INFO, USERS_MESSAGES['USER_RESET_PASSWORD_SUCCESS'])
		else:
			messages.add_message(request, messages.ERROR, USERS_MESSAGES['USER_RESET_PASSWORD_FAIL'])
			return redirect('reset-password-link', token=token)

		return redirect('login')
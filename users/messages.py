USERS_MESSAGES = {
	'LOGIN_REQUIRED'			: 	'Dostęp wymaga uwierzytelnienia. Zaloguj się.',
	'USER_NOT_ACTIVE'			: 	'Twoje konto nie zostało aktywowane, aby to zrobić kliknij w link wysłany na adres email podany podczas rejestracji',
	'PROFILE_NOT_COMPLETE'		: 	'Koniecznie jest uzupełnienie profilu.',
	
	'REGISTRATION_SUCCESS'		: 	'Rejestracja przebiegła pomyślnie. Klinkij w link wysłany ma podany adres e-mail, aby aktywować swoje konto.',
	'REGISTRATION_FAIL'			: 	'Wystąpił błąd podczas procesu rejestracji, spróbuj założyć konto ponownie.',
	
	'USER_LOGIN_SUCCESS'		: 	'Zostałeś pomyślnie zalogowany.',
	'USER_LOGIN_FAIL'			:	'Podany adres email i hasło nie są poprawne. Spróbuj jeszcze raz.',
	'USER_LOGIN_ERROR'			:	'Wystąpił błąd podczas logowania, spróbuj jeszcze raz.',
	'USER_LOGIN_NOT_FOUND'		: 	'Użytkownik o podanym adresie e-mail nie istnieje!',
	
	'USER_LOGOUT'				:	'Zostałeś pomyślnie wylogowany.',
	
	'USER_ACTIVATE_SUCCESS'		: 	'Twoje konto zostało aktywowane, możesz się zalogować do serwisu.',
	'USER_ACTIVATE_ERROR'		: 	'Wystąpił błąd podczas procesu aktywacji konta użytkownika.',

	'USER_REMIND_EMAIL_SENT'	:	'Abt dokończyć proces resetowania hasła, postępuj zgodnie ze wskazówkami przesłanymi na podany adres email.',

	'USER_RESET_PASSWORD_FAIL'		: 'Wystąpił bład podczas resetowania hasła, spróbuj jeszcze raz.',
	'USER_RESET_PASSWORD_ERROR'		: 'Podany link resetujący hasło nie jest poprawny!',
	'USER_RESET_PASSWORD_SUCCESS'	: 'Hasło do Twojego konta zostało zmienione, możesz się zalogować.',

	'USER_PROFILE_CHANGE_SUCCESS': 	'Twoje dane zostały zmienione'
}
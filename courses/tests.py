from django.test import TestCase

# Create your tests here.
from datetime import datetime, timedelta
from django.contrib import auth
from django.contrib.auth import get_user_model
from django.urls import reverse
from users.models import Group
from users.models import Announcement
from django.test import Client
from django.template.defaultfilters import slugify
from django.utils.timezone import now

from .models import Course, Lecture


def create_group():
	group = Group.objects.create(name='3ID12B')
	return group

def create_user(active=False, staff=False, profile=False):
	User = get_user_model()
	user = User.objects.create_user(email='michal@mail.com', password='haslo123', is_active=active, is_staff=staff)

	if profile:
		user.first_name = 'michal'
		user.last_name = 'grudzien'
		user.index = 784557
		user.group = create_group()
		user.save()

	return user

def create_course():
	course = Course.objects.create(
		name = 'kurs1',
		description = 'opsis 1',
		updated = datetime.now(),
		created = datetime.now(),
		)
	return course


class CourseMethodTests( TestCase ):

	def test_courses_view_not_login_access(self):
		user = auth.get_user(self.client)
		response = self.client.get(reverse('courses'))
		self.assertIs(user.is_authenticated(), False)
		self.assertEqual(response.status_code, 302)

	def test_courses_view_login_access_staff(self):
		self.user = create_user(active=True, staff=True)
		client = Client()
		client.login(email='michal@mail.com', password='haslo123')
		user = auth.get_user(client)
		response = client.get(reverse('courses'))
		self.assertIs(user.is_authenticated(), True)
		self.assertEqual(response.status_code, 200)

	def test_courses_view_login_access_no_profile_completed(self):
		self.user = create_user(active=True)
		client = Client()
		client.login(email='michal@mail.com', password='haslo123')
		user = auth.get_user(client)
		response = client.get(reverse('courses'))
		self.assertIs(user.is_authenticated(), True)
		self.assertEqual(response.status_code, 302)

	def test_courses_view_login_access_profile_no_index(self):
		self.group = Group.objects.create(name='3ID12B')
		self.user = create_user(active=True)
		self.user.group = self.group
		self.user.save()
		client = Client()
		client.login(email='michal@mail.com', password='haslo123')
		user = auth.get_user(client)
		response = client.get(reverse('courses'))
		self.assertIs(user.is_authenticated(), True)
		self.assertEqual(response.status_code, 302)

	def test_courses_view_login_access_profile_no_group(self):
		self.group = create_group()
		self.user = create_user(active=True)
		self.user.index = 124545
		self.user.save()
		client = Client()
		client.login(email='michal@mail.com', password='haslo123')
		user = auth.get_user(client)
		response = client.get(reverse('courses'))
		self.assertIs(user.is_authenticated(), True)
		self.assertEqual(response.status_code, 302)

	def test_courses_view_login_access_profile_completed(self):
		self.user =create_user(active=True, profile=True)
		client = Client()
		client.login(email='michal@mail.com', password='haslo123')
		user = auth.get_user(client)
		response = client.get(reverse('courses'))
		self.assertIs(user.is_authenticated(), True)
		self.assertEqual(response.status_code, 200)

	def test_is_course_slug_generated(self):
		course = create_course()
		self.assertEqual(course.slug, slugify(course.name))


def create_lecture(password=None, active_hours=0):
	course = create_course()

	lecture = Lecture.objects.create(
		name = 'lekcja 1',
		description = 'opis lekcji 1',
		password = password,
		active_time = now() + timedelta(hours=active_hours),
		file = None,
		points_max = 10,
		course = course,
		updated = now(),
		created = now(),
	)

	return lecture


class LectureMethodsTests( TestCase ):

	def test_lecture_view_status_no_lecture_no_active_staff_member(self):
		lecture = create_lecture(active_hours=-1)
		self.user = create_user(active=True, staff=True)
		client = Client()
		client.login(email='michal@mail.com', password='haslo123')
		user = auth.get_user(client)
		slug = lecture.slug + 'abc'
		response = client.get(reverse('lecture', args=(lecture.course.slug, slug)))
		self.assertIs(user.is_authenticated(), True)
		self.assertEqual(response.status_code, 302)
		self.assertEqual(response['Location'], reverse('course', kwargs={'course_slug':lecture.course.slug,}))

	def test_lecture_view_status_no_lecture_active_staff_member(self):
		lecture = create_lecture(active_hours=1)
		self.user = create_user(active=True, staff=True)
		client = Client()
		client.login(email='michal@mail.com', password='haslo123')
		user = auth.get_user(client)
		slug = lecture.slug + 'abc'
		response = client.get(reverse('lecture', args=(lecture.course.slug, slug)))
		self.assertIs(user.is_authenticated(), True)
		self.assertEqual(response.status_code, 302)
		self.assertEqual(response['Location'], reverse('course', kwargs={'course_slug':lecture.course.slug,}))

	def test_lecture_view_status_no_lecture_no_active_logout_user(self):
		lecture = create_lecture(active_hours=-1)
		client = Client()
		user = auth.get_user(client)
		slug = lecture.slug + 'abc'
		response = client.get(reverse('lecture', args=(lecture.course.slug, slug)))
		self.assertIs(user.is_authenticated(), False)
		self.assertEqual(response.status_code, 302)

	def test_lecture_view_status_no_lecture_active_logout_user(self):
		lecture = create_lecture(active_hours=1)
		client = Client()
		user = auth.get_user(client)
		slug = lecture.slug + 'abc'
		response = client.get(reverse('lecture', args=(lecture.course.slug, slug)))
		self.assertIs(user.is_authenticated(), False)
		self.assertEqual(response.status_code, 302)

	def test_lecture_view_status_no_lecture_active_logout_user(self):
		lecture = create_lecture(active_hours=1)
		client = Client()
		user = auth.get_user(client)
		slug = lecture.slug + 'abc'
		response = client.get(reverse('lecture', args=(lecture.course.slug, slug)))
		self.assertIs(user.is_authenticated(), False)
		self.assertEqual(response.status_code, 302)

	def test_lecture_view_status_lecture_active_logout_user(self):
		lecture = create_lecture(active_hours=1)
		client = Client()
		user = auth.get_user(client)
		response = client.get(reverse('lecture', args=(lecture.course.slug, lecture.slug )))
		self.assertIs(user.is_authenticated(), False)
		self.assertEqual(response.status_code, 302)

	def test_lecture_view_status_no_active_login_user_profile_not_complete(self):
		lecture = create_lecture(active_hours=-1)
		self.user =create_user(active=True)
		client = Client()
		client.login(email='michal@mail.com', password='haslo123')
		user = auth.get_user(client)
		response = client.get(reverse('lecture', args=(lecture.course.slug, lecture.slug)))
		self.assertIs(user.is_authenticated(), True)
		self.assertEqual(response.status_code, 302)

	def test_lecture_view_status_no_active_login_user_profile_complete(self):
		lecture = create_lecture(active_hours=-1)
		self.user =create_user(active=True, profile=True)
		client = Client()
		client.login(email='michal@mail.com', password='haslo123')
		user = auth.get_user(client)
		response = client.get(reverse('lecture', args=(lecture.course.slug, lecture.slug)))
		self.assertIs(user.is_authenticated(), True)
		self.assertEqual(response.status_code, 302)

	def test_lecture_view_status_active_login_user_profile_complete(self):
		lecture = create_lecture(active_hours=1)
		self.user = create_user(active=True, profile=True)
		client = Client()
		client.login(email='michal@mail.com', password='haslo123')
		user = auth.get_user(client)
		response = client.get(reverse('lecture', args=(lecture.course.slug, lecture.slug)))
		self.assertIs(user.is_authenticated(), True)
		self.assertEqual(response.status_code, 200)

	def test_lecture_view_active_page_login_user_profile_complete(self):
		lecture = create_lecture(active_hours=1)
		self.user = create_user(active=True, profile=True)
		client = Client()
		client.login(email='michal@mail.com', password='haslo123')
		user = auth.get_user(client)
		response = client.get("%s?page=1" % reverse('lecture', kwargs={'course_slug':lecture.course.slug, 'lecture_slug':lecture.slug,}))
		self.assertIs(user.is_authenticated(), True)
		self.assertEqual(response.status_code, 200)

	def test_lecture_view_status_active_no_page_login_user_profile_complete(self):
		lecture = create_lecture(active_hours=1)
		self.user = create_user(active=True, profile=True)
		client = Client()
		client.login(email='michal@mail.com', password='haslo123')
		user = auth.get_user(client)
		response = client.get("%s?page=100" % reverse('lecture', kwargs={'course_slug':lecture.course.slug, 'lecture_slug':lecture.slug,}))
		self.assertIs(user.is_authenticated(), True)
		self.assertEqual(response.status_code, 200)

	def test_lecture_view_status_active_password_login_user_profile_complete_no_password(self):
		lecture = create_lecture(password='haslo', active_hours=1)
		self.user = create_user(active=True, profile=True)
		client = Client()
		client.login(email='michal@mail.com', password='haslo123')
		user = auth.get_user(client)
		response = client.get(reverse('lecture', args=(lecture.course.slug, lecture.slug)))
		self.assertIs(user.is_authenticated(), True)
		self.assertEqual(response.status_code, 302)

	def test_lecture_view_status_active_password_login_user_profile_complete_wrong_password(self):
		lecture = create_lecture(password='haslo', active_hours=1)
		self.user = create_user(active=True, profile=True)
		client = Client()
		client.login(email='michal@mail.com', password='haslo123')
		user = auth.get_user(client)
		session = client.session
		session['lecture_password'] = 'haslo123'
		session.save()
		response = client.get(reverse('lecture', args=(lecture.course.slug, lecture.slug)))
		self.assertIs(user.is_authenticated(), True)
		self.assertEqual(response.status_code, 302)

	def test_lecture_view_status_active_password_login_user_profile_complete_password(self):
		lecture = create_lecture(password='haslo', active_hours=1)
		self.user = create_user(active=True, profile=True)
		client = Client()
		client.login(email='michal@mail.com', password='haslo123')
		user = auth.get_user(client)

		response = client.get(reverse('lecture', args=(lecture.course.slug, lecture.slug)))
		self.assertIs(user.is_authenticated(), True)
		self.assertEqual(response.status_code, 302)
		self.assertEqual(response['Location'], reverse('lecture_password', kwargs={'course_slug' : lecture.course.slug, 'lecture_slug' : lecture.slug,}))

		response = client.post(reverse('lecture_password', kwargs={'course_slug' : lecture.course.slug, 'lecture_slug' : lecture.slug,}), )
		self.assertEqual(response.status_code, 200)
		self.assertEqual(response.context['form']['password'].errors, [u'Brak wymaganego hasła! Podaj hasło do kursu'])

		response = client.post(reverse('lecture_password', kwargs={'course_slug' : lecture.course.slug, 'lecture_slug' : lecture.slug,}), {'abc':'def'})
		self.assertEqual(response.status_code, 200)
		self.assertEqual(response.context['form']['password'].errors, [u'Brak wymaganego hasła! Podaj hasło do kursu'])

		response = client.post(reverse('lecture_password', kwargs={'course_slug' : lecture.course.slug, 'lecture_slug' : lecture.slug,}), {'password':'def'})
		self.assertEqual(response.status_code, 302)

		response = client.post(reverse('lecture_password', kwargs={'course_slug' : lecture.course.slug, 'lecture_slug' : lecture.slug,}), {'password':'a'})
		self.assertEqual(response.status_code, 200)
		self.assertEqual(response.context['form']['password'].errors, [u'Za krótkie hasło! Minimalna ilość znaków to: 3.'])

		response = client.post(reverse('lecture_password', kwargs={'course_slug' : lecture.course.slug, 'lecture_slug' : lecture.slug,}), {'password':'Lorem ipsum dolor sit amet, consectetur'})
		self.assertEqual(response.status_code, 200)
		self.assertEqual(response.context['form']['password'].errors, [u'Za długie hasło! Maksymalna ilośc znaków to: 32.'])

		response = client.post(reverse('lecture_password', kwargs={'course_slug' : lecture.course.slug, 'lecture_slug' : lecture.slug,}), {'password':'haslo'})
		self.assertIs(user.is_authenticated(), True)
		self.assertEqual(response.status_code, 302)
		self.assertEqual(response['Location'], (reverse('lecture', kwargs={'course_slug' : lecture.course.slug, 'lecture_slug' : lecture.slug,})))

	def test_lecture_view_status_no_active_staff_member(self):
		lecture = create_lecture(active_hours=-1)
		self.user = create_user(active=True, staff=True)
		client = Client()
		client.login(email='michal@mail.com', password='haslo123')
		user = auth.get_user(client)
		response = client.get(reverse('lecture', kwargs={'course_slug' : lecture.course.slug, 'lecture_slug' : lecture.slug,}))
		self.assertIs(user.is_authenticated(), True)
		self.assertEqual(response.status_code, 302)

	def test_lecture_view_status_active_staff_member(self):
		lecture = create_lecture(active_hours=1)
		self.user = create_user(active=True, staff=True)
		client = Client()
		client.login(email='michal@mail.com', password='haslo123')
		user = auth.get_user(client)
		response = client.get(reverse('lecture', args=(lecture.course.slug, lecture.slug)))
		self.assertIs(user.is_authenticated(), True)
		self.assertEqual(response.status_code, 200)

	def test_home_view_context(self):
		user = create_user()
		course = create_course()
		ann = Announcement.objects.create(
			user = user,
			course = course,
			text = 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eaque totam quas placeat, eligendi sunt minima, repellendus. Libero error, ut dolor assumenda quae. Aspernatur aliquid itaque incidunt ipsum nobis eaque, rerum?',
			updated = datetime.now(),
			created = datetime.now(),
			)
		response = self.client.get(reverse('home'))
		self.assertTrue('last_news' in response.context)
		self.assertEqual([ann.pk for ann in response.context['last_news']], [1])
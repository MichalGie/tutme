# -*- coding: utf-8 -*-
# Generated by Django 1.11.5 on 2018-01-07 09:38
from __future__ import unicode_literals

import ckeditor.fields
import datetime
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Course',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=128, verbose_name='Nazwa')),
                ('slug', models.SlugField(blank=True, null=True, verbose_name='Slug')),
                ('description', models.CharField(max_length=255, verbose_name='Opis')),
                ('password', models.CharField(blank=True, max_length=16, null=True, verbose_name='Hasło')),
                ('cover', models.ImageField(blank=True, null=True, upload_to='coures/covers/', verbose_name='Okładka')),
                ('updated', models.DateTimeField(auto_now=True, verbose_name='Modyfikowano')),
                ('created', models.DateTimeField(auto_now_add=True, verbose_name='Utworzono')),
            ],
            options={
                'verbose_name_plural': 'Kursy',
                'verbose_name': 'Kurs',
            },
        ),
        migrations.CreateModel(
            name='Lecture',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=128, verbose_name='Nazwa')),
                ('slug', models.SlugField(blank=True, null=True, verbose_name='Slug')),
                ('description', models.CharField(max_length=255, verbose_name='Opis')),
                ('password', models.CharField(blank=True, max_length=32, null=True, verbose_name='Hasło')),
                ('active_time', models.DateTimeField(default=datetime.date(2018, 1, 15), verbose_name='Aktywna do')),
                ('file', models.FileField(blank=True, null=True, upload_to='uploads/courses/files/', verbose_name='Plik od lekcji')),
                ('points_max', models.PositiveSmallIntegerField(default=10, verbose_name='Max. pkt. za lekcje')),
                ('updated', models.DateTimeField(auto_now=True, verbose_name='Modyfikowano')),
                ('created', models.DateTimeField(auto_now_add=True, verbose_name='Utworzono')),
            ],
            options={
                'verbose_name_plural': 'Zajęcia',
                'verbose_name': 'Zajęcia',
            },
        ),
        migrations.CreateModel(
            name='Page',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('number', models.PositiveSmallIntegerField(verbose_name='Numer strony')),
                ('content', ckeditor.fields.RichTextField(verbose_name='Treść')),
                ('updated', models.DateTimeField(auto_now=True, verbose_name='Modyfikowano')),
                ('created', models.DateTimeField(auto_now_add=True, verbose_name='Utworzono')),
            ],
            options={
                'verbose_name_plural': 'Strony lekcji',
                'verbose_name': 'Strona lekcji',
            },
        ),
        migrations.CreateModel(
            name='Task',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('number', models.CharField(blank=True, max_length=8, null=True, verbose_name='Numer')),
                ('content', models.CharField(max_length=255, verbose_name='Treść')),
            ],
            options={
                'verbose_name_plural': 'Zadania',
                'verbose_name': 'Zadanie',
            },
        ),
        migrations.CreateModel(
            name='UserTaskDetail',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('result', models.TextField(verbose_name='Wynik')),
                ('status', models.CharField(choices=[('P', 'Oczekuje'), ('S', 'Przesłano'), ('A', 'Zaakceptowano'), ('D', 'Odrzucono')], default='P', max_length=1, verbose_name='Status')),
                ('updated', models.DateTimeField(auto_now=True, verbose_name='Modyfikowano')),
                ('created', models.DateTimeField(auto_now_add=True, verbose_name='Utworzono')),
                ('task', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='user_task', to='courses.Task', verbose_name='Zadanie')),
            ],
            options={
                'verbose_name_plural': 'Zadania użytkowników',
                'verbose_name': 'Zadanie użytkownika',
            },
        ),
    ]

# -*- coding: utf-8 -*-
# Generated by Django 1.11.5 on 2018-01-19 07:51
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('courses', '0003_auto_20180107_0957'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='course',
            options={'verbose_name': '1. Kurs', 'verbose_name_plural': '1. Kursy'},
        ),
        migrations.AlterModelOptions(
            name='lecture',
            options={'verbose_name': '2. Zajęcia', 'verbose_name_plural': '2. Zajęcia'},
        ),
        migrations.AlterModelOptions(
            name='page',
            options={'verbose_name': '3. Strona lekcji', 'verbose_name_plural': '3. Strony lekcji'},
        ),
        migrations.AlterModelOptions(
            name='task',
            options={'verbose_name': '4. Zadanie', 'verbose_name_plural': '4. Zadania'},
        ),
        migrations.AlterModelOptions(
            name='usertaskdetail',
            options={'verbose_name': '5. Zadanie użytkownika', 'verbose_name_plural': '5. Zadania użytkowników'},
        ),
        migrations.AlterField(
            model_name='usertaskdetail',
            name='task',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='user_task', to='courses.Task', verbose_name='Zadanie'),
        ),
    ]

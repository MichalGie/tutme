# -*- coding: utf-8 -*-
# Generated by Django 1.11.5 on 2018-01-07 09:38
from __future__ import unicode_literals

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('courses', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='usertaskdetail',
            name='user',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL, verbose_name='Użytkownik'),
        ),
        migrations.AddField(
            model_name='task',
            name='lecture',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='task', to='courses.Lecture', verbose_name='Lekcja'),
        ),
        migrations.AddField(
            model_name='page',
            name='lecture',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='pages', to='courses.Lecture', verbose_name='Lekcja'),
        ),
        migrations.AddField(
            model_name='lecture',
            name='course',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='course_lecture', to='courses.Course', verbose_name='Kurs'),
        ),
    ]

from django.db import models
from django.db.models.query import QuerySet
from ckeditor_uploader.fields import RichTextUploadingField
from datetime import datetime
from django.utils.timezone import now
from django.template.defaultfilters import slugify

# Create your models here.
from django.contrib.auth import get_user_model
from django.conf import settings

class CustomQuerySetManager(models.Manager):
	def __getattr__(self, attr, *args):
		try:
			return getattr(self.__class__, attr, *args)
		except AttributeError:
			if attr.startswith('__') and attr.endswith('__'):
				raise
			return getattr(self.get_query_set(), attr, *args)

	def get_query_set(self):
		return self.model.QuerySet(self.model, using=self._db)

class Course(models.Model):
	name 		= models.CharField(max_length=128, blank=False, null=False, verbose_name='Nazwa')
	slug 		= models.SlugField(blank=True, null=True, verbose_name='Slug')
	description = models.CharField(max_length=255, verbose_name='Opis')
	password 	= models.CharField(max_length=16, blank=True, null=True, verbose_name='Hasło')
	cover 		= models.ImageField(upload_to='coures/covers/', blank=True, null=True, verbose_name='Okładka')

	updated 	= models.DateTimeField(auto_now=True, verbose_name='Modyfikowano')
	created 	= models.DateTimeField(auto_now_add=True, verbose_name='Utworzono')

	objects 	= CustomQuerySetManager()

	class Meta:
		verbose_name = 'Kurs'
		verbose_name_plural = '1. Kursy'

	def __str__(self):
		return self.name

	def save(self, *args, **kwargs):
		self.slug = slugify(self.name)
		super(Course, self).save(*args, **kwargs)

	def get_max_points(self):
		lectures = self.course_lecture.all()

		max_points = 0
		for lecture in lectures:
			max_points += lecture.points_max

		return max_points


	class QuerySet(QuerySet):
		def registered_on_user(self, user):
			return self.filter(user_courses__user=user)

class CustomLectureManager(models.Manager):
	def get_queryset(self):
		return super(CustomLectureManager, self).get_queryset().filter(active_time__gte=now())

class Lecture(models.Model):
	name 			= models.CharField(max_length=128, blank=False, null=False, verbose_name='Nazwa')
	slug 			= models.SlugField(blank=True, null=True, verbose_name='Slug')
	description 	= models.CharField(max_length=255, verbose_name='Opis')

	password 		= models.CharField(max_length=32, blank=True, null=True, verbose_name='Hasło')
	active_time 	= models.DateTimeField(default=datetime(2018, 1, 15, 12, 00), verbose_name='Aktywna do')

	file 			= models.FileField(upload_to='uploads/courses/files/', blank=True, null=True, verbose_name='Plik do zajęc')

	points_max 		= models.PositiveSmallIntegerField(default=10, blank=False, null=False, verbose_name='Max. pkt. za zajęcia')

	course 			= models.ForeignKey('Course', related_name='course_lecture', on_delete=models.CASCADE, verbose_name='Kurs')

	updated 		= models.DateTimeField(auto_now=True, verbose_name='Modyfikowano')
	created 		= models.DateTimeField(auto_now_add=True, verbose_name='Utworzono')

	objects 		= models.Manager()
	active_objects 	= CustomLectureManager()

	class Meta:
		verbose_name = 'Zajęcia'
		verbose_name_plural = '2. Zajęcia'

	def __str__(self):
		return self.name

	def is_active(self):
		if self.active_time >= now():
			return True
		else:
			return False
	is_active.short_description = 'Aktywny'

	def save(self, *args, **kwargs):
		self.slug = slugify(self.name)
		super(Lecture, self).save(*args, **kwargs)


class Page(models.Model):
	number 	= models.PositiveSmallIntegerField(blank=False, null=False, verbose_name='Numer strony')
	content = RichTextUploadingField(blank=False, null=False, verbose_name='Treść')

	lecture = models.ForeignKey('Lecture', related_name='pages', on_delete=models.CASCADE, verbose_name='Zajęcia')

	updated = models.DateTimeField(auto_now=True, verbose_name='Modyfikowano')
	created = models.DateTimeField(auto_now_add=True, verbose_name='Utworzono')

	class Meta:
		verbose_name = 'Strona zajęć'
		verbose_name_plural = '3. Strony zajęć'

	def __str__(self):
		return self.lecture.name + '(str. ' + str(self.number) + ')'

	def get_course(self):
		return self.lecture.course.name


class Task(models.Model):
	number 		= models.CharField(max_length=8, blank=False, null=False,  verbose_name='Numer')
	content		= models.CharField(max_length=255, blank=False, null=False,  verbose_name='Treść')
	lecture 	= models.ForeignKey(Lecture, related_name='task', on_delete=models.CASCADE,  verbose_name='Zajęcia')

	class Meta:
		verbose_name = 'Zadanie'
		verbose_name_plural = '4. Zadania'

	def __str__(self):
		return self.number


UT_PENDING 			= 'P'
UT_SENDED 			= 'S'
UT_APPROVED 		= 'A'
UT_DENIED 			= 'D'

USER_TASK_STATUS = (
		(UT_PENDING, 'Oczekuje'),
		(UT_SENDED, 'Przesłano'),
		(UT_APPROVED, 'Zaakceptowano'),
		(UT_DENIED, 'Odrzucono'),
	)

class UserTaskDetail(models.Model):
	task 	= models.ForeignKey(Task, related_name='user_task', on_delete=models.CASCADE, blank=True, null=True, verbose_name='Zadanie')
	user 	= models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE, verbose_name='Użytkownik')

	result 	= models.TextField(blank=False, null=True, verbose_name='Wynik')
	status 	= models.CharField(choices=USER_TASK_STATUS, max_length=1, default=UT_PENDING, blank=False, null=False, verbose_name='Status')

	updated = models.DateTimeField(auto_now=True, verbose_name='Modyfikowano')
	created = models.DateTimeField(auto_now_add=True, verbose_name='Utworzono')

	class Meta:
		verbose_name = 'Zadanie użytkownika'
		verbose_name_plural = '5. Zadania użytkowników'

	def get_lecture(self):
		return self.task.lecture
	get_lecture.short_description = 'Zajęcia'
import pdfkit
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.core.mail import EmailMultiAlternatives
from django.core.urlresolvers import reverse
from django.contrib import messages
from django.contrib.admin.views.decorators import staff_member_required
from django.db.models import Q
from django.http import Http404, HttpResponse
from django.shortcuts import render, redirect
from django.utils.dateformat import format
from django.utils.decorators import method_decorator
from django.utils.timezone import now
from django.template import Context
from django.template.loader import get_template
from django.views import View
from django.views.generic.edit import CreateView, FormView, UpdateView
from django.views.generic.list import ListView
# Create your views here.

from .forms import RegisterUserToCourseForm, LecturePasswordForm, SendEmailCourseForm, UserTaskDetaiCreatelForm
from .models import Course, Lecture, Page, UserTaskDetail
from users.forms import UserLectureDetailForm
from users.models import UserCourseDetail, UserLectureDetail, Announcement
from users.views import CustomLoginActivateRequiredMixin
from .messages import MESSAGES_COURSES


class CourseAccessMixin(CustomLoginActivateRequiredMixin, View):
	def dispatch(self, request, *args, **kwargs):
		if request.user.is_authenticated():
			user = request.user
			course_slug = self.kwargs.get('course_slug')

			try:
				course = Course.objects.get(slug=course_slug)
			except:
				messages.add_message(request, messages.ERROR, MESSAGES_COURSES['COURSE_NOT_FOUND'])
				return redirect('courses')

			try:
				access = UserCourseDetail.objects.get(user=user, course__slug=course.slug)
			except:
				if course.password:
					messages.add_message(request, messages.ERROR, MESSAGES_COURSES['COURSE_NO_ACCESS'])
				return redirect('course_access', course_slug = course.slug)

			return super(CourseAccessMixin, self).dispatch(request, *args, **kwargs)

		return redirect('/')
				

class HomeView(View):
	def get(self, request, *args, **kwargs):
		
		announcements = Announcement.objects.all()[:2]
		
		context = {
			'last_news' : announcements,
		}

		return render(request, 'main.html', context)


class RegisterUserToCourse(FormView):

	form_class = RegisterUserToCourseForm

	template_name = 'courses/course_access.html'

	def dispatch(self, request, *args, **kwargs):
		try:
			course = Course.objects.get(slug=self.kwargs.get('course_slug'))
		except:
			messages.add_message(request, messages.ERROR, MESSAGES_COURSES['COURSE_NOT_FOUND'])
			return redirect('courses')

		try:
			access = UserCourseDetail.objects.get(user=request.user, course=course)
			return redirect('course', course_slug=course.slug)
		except:
			if course.password is None:
				messages.add_message(request, messages.INFO, MESSAGES_COURSES['COURSE_ACCESS_SUCCESS'])
				UserCourseDetail.objects.create(user=request.user, course=course)
				return redirect('course', course_slug=course.slug)
			return super(RegisterUserToCourse, self).dispatch(request, *args, **kwargs)

		return super(RegisterUserToCourse, self).dispatch(request, *args, **kwargs)

	def get_context_data(self, *args, **kwargs):
		context = super(RegisterUserToCourse, self).get_context_data(*args, **kwargs)
		
		course_slug =  self.kwargs.get('course_slug')
		course = Course.objects.get(slug=course_slug)
		context['course'] = course
		return context

	def form_valid(self, form, *args, **kwargs):
		if form.is_valid():
			password = form.cleaned_data['password']
			course_slug = self.kwargs.get('course_slug')

			course = Course.objects.get(slug=course_slug)
	
			if course.password:
				if course.password != password:
					messages.add_message(self.request, messages.ERROR, MESSAGES_COURSES['COURSE_WRONG_PASSWORD'])
					raise forms.ValidationError("Niepoprawne hasło!")

			UserCourseDetail.objects.create(user=self.request.user, course=course)
			messages.add_message(self.request, messages.INFO, MESSAGES_COURSES['COURSE_ACCESS_SUCCESS'])
		return super(RegisterUserToCourse, self).form_valid(form, *args, **kwargs)


	def get_success_url(self):
		course_slug = self.kwargs.get('course_slug')
		return reverse('course', kwargs={'course_slug':course_slug,})


class CoursesListView(CustomLoginActivateRequiredMixin, ListView):
	model = Course

	def get_context_data(self, **kwargs):
		context = super(CoursesListView, self).get_context_data(**kwargs)
		user_courses = Course.objects.registered_on_user(self.request.user)

		context['user_courses'] = user_courses
		return context


class LecturesListView(CourseAccessMixin, ListView):
	def get_queryset(self):
		slug = self.kwargs.get('course_slug')
		queryset = Lecture.objects.filter(course__slug=slug)
		return queryset

	def get_context_data(self, **kwargs):
		context = super(LecturesListView, self).get_context_data(**kwargs)
		lectures = context.get('object_list')

		course_slug = self.kwargs.get('course_slug')
		lecture_name = Course.objects.get(slug=course_slug)
		
		context['lecture_name'] = lecture_name.name

		for lecture in lectures:
			try:
				obj = UserLectureDetail.objects.get(user=self.request.user, lecture=lecture)
				lecture.completed = True
				lecture.status = obj.get_status_display()
			except:
				lecture.completed = False
		return context


class LecturePasswordFormView(CourseAccessMixin, FormView):
	form_class =  LecturePasswordForm
	template_name = 'courses/lecture_password_form.html'


	def get_context_data(self, *args, **kwargs):
		context = super(LecturePasswordFormView, self).get_context_data(*args, **kwargs)

		lecture_slug =  self.kwargs.get('lecture_slug')
		lecture = Lecture.objects.get(slug=lecture_slug)
		context['lecture'] = lecture.name
		return context

	def get_success_url(self, **kwargs):
		course_slug = self.kwargs.get('course_slug')
		lecture_slug = self.kwargs.get('lecture_slug')
		return reverse('lecture', args=(course_slug, lecture_slug))

	def form_valid(self, form):
		if form.is_valid():
			password = form.cleaned_data.get('password')
			self.request.session['lecture_password'] = password
		return super(LecturePasswordFormView, self).form_valid(form=form)


class LectureAccessMixin(CourseAccessMixin, View):
	def dispatch(self, request, *args, **kwargs):
		course_slug = self.kwargs.get('course_slug')
		lecture_slug = self.kwargs.get('lecture_slug')

		try:
			lecture = Lecture.objects.get(slug=lecture_slug, course__slug=course_slug)
		except:
			messages.add_message(request, messages.ERROR, MESSAGES_COURSES['LECTURE_NOT_FOUND'])
			return redirect('course', course_slug = course_slug)

		if lecture.password:
			try:
				password = self.request.session['lecture_password']
			except:
				return redirect('lecture_password', course_slug = course_slug, lecture_slug=lecture_slug)

			if password == lecture.password:
				return super(LectureAccessMixin, self).dispatch(request, *args, **kwargs)
			else:
				del(self.request.session['lecture_password'])
				messages.add_message(request, messages.ERROR, MESSAGES_COURSES['LECTURE_WRONG_PASSWORD'])
				return redirect('lecture_password', course_slug = course_slug, lecture_slug=lecture_slug)
		else:
			return super(LectureAccessMixin, self).dispatch(request, *args, **kwargs)


class LecturePageView(LectureAccessMixin, View):
	def get(self, request, course_slug, lecture_slug, *args, **kwargs):

		try:
			lecture = Lecture.objects.get(slug=lecture_slug)
		except:
			messages.add_message(request, messages.ERROR, MESSAGES_COURSES['LECTURE_NOT_FOUND'])
			return redirect('course', course_slug=course_slug)

		try:
			lecture = Lecture.active_objects.get(slug=lecture_slug)
		except:
			messages.add_message(request, messages.ERROR, MESSAGES_COURSES['LECTURE_ACCESS_DENIED'])
			return redirect('/')


		pages = lecture.pages.order_by('id')
		paginator = Paginator(pages, 1)
		page = request.GET.get('page')

		try:
			content = paginator.page(page)
		except PageNotAnInteger:
			content = paginator.page(1)
		except EmptyPage:
			content = paginator.page(paginator.num_pages)

		context = {
			'content' : content,
		}

		try:
			if int(page) == paginator.num_pages:
				try:
					lecture = Lecture.active_objects.get(slug=lecture_slug)
					obj = UserLectureDetail.objects.get(course_detail__user=request.user, lecture=lecture)
					form = UserLectureDetailForm(instance=obj)
					context['form'] = form
				except:
					context['form'] = UserLectureDetailForm()
		except:
			pass

		return render(request, 'courses/page_detail.html', context)


	def post(self, request, course_slug, lecture_slug, *args, **kwargs):

		form = UserLectureDetailForm(request.POST or None)
		if form.is_valid():
			new_obj = form.save(commit=False)

			user_course_detail = UserCourseDetail.objects.get(user=request.user, course__slug=course_slug)

			try:
				obj = UserLectureDetail.objects.get(course_detail=user_course_detail, lecture__slug=lecture_slug)
				obj.task = new_obj.task
				obj.save()
			except:
				new_obj.course_detail = user_course_detail
				new_obj.lecture = Lecture.active_objects.get(slug=lecture_slug)
				new_obj.save()

			messages.add_message(request, messages.SUCCESS, MESSAGES_COURSES['LECTURE_COMPLETED'])
		else:
			messages.add_message(request, messages.ERROR, MESSAGES_COURSES['LECTURE_NO_COMPLETED'])

		return redirect('page-done', course_slug=course_slug, lecture_slug=lecture_slug)


class LecturePageDoneView(LectureAccessMixin, View):
	def get(self, request,  course_slug, lecture_slug, *args, **kwargs):

		form = SendEmailCourseForm(initial={'email':request.user.email,})
		obj = Lecture.objects.get(slug=lecture_slug)
		try:
			file = obj.file
		except:
			file = None

		context = {
			'form' : form,
			'file' : file,
		}

		return render(request, 'courses/page_done.html', context)

	def post(self, request, course_slug, lecture_slug, *args, **kwargs):
		form = SendEmailCourseForm(request.POST or None)

		if form.is_valid():
			email = form.cleaned_data['email']

			lecture = Lecture.objects.get(slug=lecture_slug)

			plaintext = get_template('emails/lecture_content.txt')
			htmly     = get_template('emails/lecture_content.html')

			d = {
				'course'	: lecture.course.name,
				'lecture'	: lecture.name,
				'user_name' : request.user.first_name,
				}

			subject 	= 'Materiały z lekcji {}'.format(lecture.name)
			from_email 	= 'admin@tutme.pl'
			to 			= form.cleaned_data['email']

			text_content = plaintext.render(d)
			html_content = htmly.render(d)
			msg = EmailMultiAlternatives(subject, text_content, from_email, [to])
			msg.attach_alternative(html_content, "text/html")
			msg.attach_file(lecture.file.path)
			msg.send()

			messages.add_message(request, messages.INFO, MESSAGES_COURSES['LECTURE_EMAIL_SENDED'])
			return redirect('/')

			messages.add_message(request, messages.ERROR, MESSAGES_COURSES['LECTURE_EMAIL_ERROR'])
		return render(request, 'courses/page_done.html', {})


class AnnouncementView(ListView):
	model = Announcement


class GenerateCourseRaport(View):

	@method_decorator(staff_member_required)
	def get(self, request, course_slug, *args, **kwargs):
		course = Course.objects.get(slug=course_slug)

		context = {
			'course'	: course,
		}

		user_course_detail = UserCourseDetail.objects.filter(course=course)
		context['users_course'] = user_course_detail

		return render(request, 'reports/course.html', context)

	@method_decorator(staff_member_required)
	def post(self, request, course_slug, *args, **kwargs):
		course = Course.objects.get(slug=course_slug)

		context = {
			'course'	: course,
		}

		user_course_detail = UserCourseDetail.objects.filter(course=course)
		context['users_course'] = user_course_detail

		template = get_template('reports/course.html')
		html = template.render(context)
		pdf = pdfkit.from_string(html, False)

		response = HttpResponse(pdf,content_type='application/pdf')
		response['Content-Disposition'] = 'attachment; filename="{}.pdf"'.format('Raport z kursu '  + course.name + ' ' + format(now(), 'd.m.Y'))

		return response


class TaskCreateView(CustomLoginActivateRequiredMixin, CreateView):
	model = UserTaskDetail
	redirect_to = ''
	form_class = UserTaskDetaiCreatelForm
	template_name = 'courses/user_task_detail_create_form.html'

	def dispatch(self, request, *args, **kwargs):
		try:
			lecture_slug = self.kwargs.get('lecture_slug')
			obj = UserTaskDetail.objects.get(user=self.request.user, task__lecture__slug=lecture_slug)
			course_slug = self.kwargs.get('course_slug')
			return redirect('task', course_slug=course_slug, lecture_slug=lecture_slug)
		except:
			return super(TaskCreateView, self).dispatch(request, *args, **kwargs)

	def get_success_url(self):
		return reverse('profile')

	def get_form_kwargs(self):
		lecture_slug = self.kwargs.get('lecture_slug')
		kwargs = super(TaskCreateView, self).get_form_kwargs()
		kwargs['lecture_slug'] = lecture_slug
		return kwargs 

	def get_context_data(self, *args, **kwargs):
		lecture_slug = self.kwargs.get('lecture_slug')
		context = super(TaskCreateView, self).get_context_data(*args, **kwargs)
		lecture  = Lecture.objects.get(slug=lecture_slug)
		context['lecture'] = lecture
		return context

	def form_valid(self, form, *args, **kwargs):
		instance = form.save(commit=False)
		instance.user = self.request.user
		instance.save()
		return super(TaskCreateView, self).form_valid(form, *args, **kwargs)


class TaskUpdateView(CustomLoginActivateRequiredMixin, UpdateView):
	model = UserTaskDetail
	redirect_to = ''
	fields = ['result',]
	template_name = 'courses/user_task_detail_update_form.html'

	def dispatch(self, request, *args, **kwargs):
		try:
			lecture_slug = self.kwargs.get('lecture_slug')
			obj = UserTaskDetail.objects.get(user=self.request.user, task__lecture__slug=lecture_slug)
			return super(TaskUpdateView, self).dispatch(request, *args, **kwargs)
		except:
			course_slug = self.kwargs.get('course_slug')
			lecture_slug = self.kwargs.get('lecture_slug')
			return redirect('task-create', course_slug=course_slug, lecture_slug=lecture_slug)

	def get_object(self):
		lecture_slug = self.kwargs.get('lecture_slug')
		obj = UserTaskDetail.objects.get(user=self.request.user, task__lecture__slug=lecture_slug)
		return obj

	def get_context_data(self, *args, **kwargs):
		lecture_slug = self.kwargs.get('lecture_slug')
		context = super(TaskUpdateView, self).get_context_data(*args, **kwargs)

		task =  UserTaskDetail.objects.get(
				Q(user=self.request.user) & Q(task__lecture__slug=lecture_slug)
			)

		context['task'] = task
		context['lecture'] = task.task.lecture

		return context

	def form_valid(self, form, *args, **kwargs):
		instance = form.save(commit=False)
		instance.status = 'S'
		instance.save()
		return super(TaskUpdateView, self).form_valid(form, *args, **kwargs)

	def get_success_url(self):
		return reverse('profile')
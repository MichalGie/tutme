from django.contrib import admin

from .models import Course, Page, Lecture, Task, UserTaskDetail
# Register your models here.


class CustomCourseAdmin(admin.ModelAdmin):
	readonly_fields = ('updated', 'created')
	list_display = ('name',)

	fieldsets = (
		(None, {
			'fields': ('name', 'description', 'password', 'cover')
		}),
		('Generowane automatycznie', {
			'classes': ('collapse',),
			'fields': ('slug', 'updated', 'created'),
		}),
	)

class CustomLectureAdmin(admin.ModelAdmin):
	readonly_fields = ('updated', 'created',)
	list_display = ('name','course','is_active',)

	fieldsets = (
		(None, {
			'fields': ('name', 'course', 'description', 'password', 'active_time', 'points_max', 'file')
		}),
		('Generowane automatycznie', {
			'classes': ('collapse',),
			'fields': ('slug', 'updated', 'created'),
		}),
	)


class CustomPageAdmin(admin.ModelAdmin):
	readonly_fields = ('updated', 'created',)
	list_display = ('__str__','get_course',)

	fieldsets = (
		(None, {
			'fields': ('number', 'lecture', 'content')
		}),
		('Generowane automatycznie', {
			'classes': ('collapse',),
			'fields': ('updated', 'created'),
		}),
	)

	class Meta:
		js = (
			'js/mathajx.js'
		)

class CustomTaskDetailAdmin(admin.ModelAdmin):
	readonly_fields = ('updated', 'created',)
	list_display = ('task', 'get_lecture', 'user','status',)

	fieldsets = (
		(None, {
			'fields': ('task', 'user', 'result', 'status')
		}),
		('Generowane automatycznie', {
			'classes': ('collapse',),
			'fields': ('updated', 'created'),
		}),
	)

class CustomTaskAdmin(admin.ModelAdmin):
	list_display = ('number','lecture')

	fieldsets = (
		(None, {
			'fields': ('number', 'lecture', 'content')
		}),
	)

admin.site.register(Course, CustomCourseAdmin)
admin.site.register(Lecture, CustomLectureAdmin)
admin.site.register(Page, CustomPageAdmin)
admin.site.register(Task, CustomTaskAdmin)
admin.site.register(UserTaskDetail, CustomTaskDetailAdmin)
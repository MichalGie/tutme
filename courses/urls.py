from django.conf.urls import url

from .views import CoursesListView, LecturesListView, LecturePageView, AnnouncementView, LecturePageDoneView, LecturePasswordFormView, RegisterUserToCourse, GenerateCourseRaport, TaskUpdateView,  TaskCreateView

urlpatterns = [
	url(r'^announcements/$', AnnouncementView.as_view(), name='announcements'),
	url(r'^password/(?P<course_slug>[\w-]+)/(?P<lecture_slug>[\w-]+)/$', LecturePasswordFormView.as_view(), name='lecture_password'),
	url(r'^register-to/(?P<course_slug>[\w-]+)/$', RegisterUserToCourse.as_view(), name='course_access'),
	url(r'^report/(?P<course_slug>[\w-]+)/$', GenerateCourseRaport.as_view(), name='generate-course-raport'),
	url(r'^(?P<course_slug>[\w-]+)/(?P<lecture_slug>[\w-]+)/done/$', LecturePageDoneView.as_view(), name='page-done'),
	url(r'^(?P<course_slug>[\w-]+)/(?P<lecture_slug>[\w-]+)/task/create$', TaskCreateView.as_view(), name='task-create'),
	url(r'^(?P<course_slug>[\w-]+)/(?P<lecture_slug>[\w-]+)/task/$', TaskUpdateView.as_view(), name='task'),
	url(r'^(?P<course_slug>[\w-]+)/(?P<lecture_slug>[\w-]+)/$', LecturePageView.as_view(), name='lecture'),
	url(r'^(?P<course_slug>[\w-]+)/$', LecturesListView.as_view(), name='course'),
	url(r'^$', CoursesListView.as_view(), name='courses'),
]
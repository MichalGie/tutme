from django import forms
from django.core.validators import  MinLengthValidator, MaxLengthValidator
from django.forms import ModelForm
from django.forms.widgets import PasswordInput, EmailInput
from .models import UserTaskDetail, Task

class LecturePasswordForm(forms.Form):
	password = forms.CharField(widget=PasswordInput,
		validators=[
			MinLengthValidator(3, "Za krótkie hasło! Minimalna ilość znaków to: 3."),
			MaxLengthValidator(32, "Za długie hasło! Maksymalna ilośc znaków to: 32."),
			],
		required=True,
		error_messages={'required': 'Brak wymaganego hasła! Podaj hasło do kursu'}
		)

class SendEmailCourseForm(forms.Form):
	email 	= forms.CharField(widget=EmailInput, required=True)

class RegisterUserToCourseForm(forms.Form):
	password = forms.CharField(widget=PasswordInput,
	validators=[
		MinLengthValidator(3, "Za krótkie hasło! Minimalna ilość znaków to: 3."),
		MaxLengthValidator(32, "Za długie hasło! Maksymalna ilośc znaków to: 32."),
		],
	required=True,
	error_messages={'required': 'Brak wymaganego hasła! Podaj hasło do kursu'}
	)

class UserTaskDetaiCreatelForm(forms.ModelForm):
	def __init__(self,lecture_slug,*args,**kwargs):
		super (UserTaskDetaiCreatelForm,self ).__init__(*args,**kwargs)
		self.fields['task'].queryset = Task.objects.filter(lecture__slug=lecture_slug)

	class Meta:
		model = UserTaskDetail
		fields = ['task',]
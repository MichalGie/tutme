MESSAGES_COURSES = {
	'COURSE_NOT_FOUND'		: 	'Nie znaleziono pożądanego kursu!',
	'COURSE_NO_ACCESS'		: 	'Nie masz dostępu do tego kursu!',
	'COURSE_WRONG_PASSWORD' : 	'Podano nieprawidłowe hasło!',
	'COURSE_ACCESS_SUCCESS'	: 	'Zostałeś zapisany na kurs.',

	'LECTURE_NOT_FOUND'		: 	'Nie znaleziono pożądanej lekcji!',
	'LECTURE_ACCESS_DENIED' : 	'Nie masz dostępu do tej lekcji! Podaj hasło dostępu.',
	'LECTURE_NO_ACCESS'		: 	'Nie masz dostępu do tej lekcji!',
	'LECTURE_WRONG_PASSWORD': 	'Nieprawidłowe hasło! Spróbuj jeszcze raz.',
	'LECTURE_COMPLETED'		:	'Lekcja ukończona pomyślnie',
	'LECTURE_NO_COMPLETED' 	:	'Lekcja nie zaliczona!',
	'LECTURE_EMAIL_SENDED'	: 	'Wiadomość email została wysłana na podany przez Ciebie adres.',
	'LECTURE_EMAIL_ERROR'	: 	'Wystąpił bład!, Spróbuj jeszcze raz.'
}